# azure-tokyo-template
You can use the Azure template in tokyo_template branch to copy my network configuration,
make sure you add SSH connection.
There WILL BE changes.

# First make sure tokyo-webapp.py's properties are correct on db_config!
nano tokyo-webapp.py
change db_config accordingly.

# Instructions to install tokyo-webapp
cd azure-tokyo-template
sudo apt install python3 python3-pip
pip install flask psycopg2
python3 -m pip install psycopg2-binary
python3 -m flask --app ./tokyo-webapp.py run --host=0.0.0.0

# Here are some DB steps you might want to following
Make sure you adjust these steps as needed
sudo nano /etc/postgresql/*/main/postgresql.conf
listen_addresses = '*'

sudo -u postgres psql template1
ALTER USER postgres with encrypted password 'your_password';
sudo nano /etc/postgresql/*/main/pg_hba.conf
host template1       postgres        all        scram-sha-256
sudo systemctl restart postgresql.service



# Make sure your DB has the following table
CREATE TABLE data (
   id serial PRIMARY KEY,
   name VARCHAR NOT NULL,
   value INTEGER NOT NULL,
   time TIMESTAMP NOT NULL
);
